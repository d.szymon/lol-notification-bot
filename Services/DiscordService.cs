﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using LeagueBot.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace LeagueBot.Services
{
    public class DiscordService
    {
        private readonly DiscordSocketClient client;
        private readonly string token;
        private readonly ILogger<DiscordService> logger;
        private readonly OpGgSettings opGgSettings;
        private readonly Dictionary<string, ulong> channelIds = new();

        public DiscordService(DiscordSettings settings, ILogger<DiscordService> logger, IOptions<OpGgSettings> opGgSettings)
        {
            this.logger = logger;
            this.opGgSettings = opGgSettings.Value;
            this.client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                AlwaysDownloadUsers = false,
            });
            this.token = settings.Token;
        }

        public async Task Init()
        {
            this.client.GuildAvailable += HandleGuild;
            await this.client.LoginAsync(TokenType.Bot, this.token);
            await this.client.StartAsync();
        }

        protected Task HandleGuild(SocketGuild guild)
        {
            foreach (var channel in guild.Channels)
            {
                foreach (var server in opGgSettings.Servers.Where(server => channel.Name.ToLower().Trim().Equals(server.Value.Channel.Trim().ToLower())))
                {
                    channelIds.Add(server.Key, channel.Id);
                }
            }
            
            this.logger.LogInformation("Zarejestrowano kanały");
            return Task.CompletedTask;
        }

        public async Task SendRankNotification(OpGgListPlayer summoner, LeagueDto league)
        {
            await (this.client.GetChannel(channelIds[summoner.Region]) as ITextChannel).SendMessageAsync(
                embed: new EmbedBuilder()
                {
                    Title = "Ranga Iron IV została osiągnięta",
                    Color = Color.Orange
                }
                    .AddField("Nazwa", summoner.Name)
                    .AddField("Region", summoner.Region)
                    .AddField("Wygrane", league.Wins)
                    .AddField("Przegrane", league.Losses)
                    .AddField("Ratio", league.WinRatio + "%")
                    .AddField("LP", league.LeaguePoints)
                    .AddField("Ranga", summoner.Rank.Rank)
                    .Build()
            );
        }

        public Task SendCompletedNotification(string region, uint page)
        {
            return (this.client.GetChannel(channelIds[region]) as ITextChannel).SendMessageAsync(
                $"Zakończono sprawdzanie regionu: {region} ({page}/{page}) stron");
        }
    }
}