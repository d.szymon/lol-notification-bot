﻿using System;
using System.Net.Http;
using LeagueBot.Models;

namespace LeagueBot.Services
{
    public class RiotApiService
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly string apiKey;

        public RiotApiService(IHttpClientFactory httpClientFactory, RiotApiSettings settings)
        {
            this.httpClientFactory = httpClientFactory;
            this.apiKey = settings.ApiKey;
        }

        protected HttpClient GetClient()
        {
            var client = this.httpClientFactory.CreateClient(nameof(RiotApiService));
            client.DefaultRequestHeaders.Add("X-Riot-Token", this.apiKey);
            return client;
        }

        public HttpClient GetClient(string region)
        {
            var client = GetClient();
            client.BaseAddress = new Uri(GetApiUrlFromRegion(region));
            return client;
        }
        
        public static string GetApiUrlFromRegion(string region)
        {
            return $"https://{region.ToLower()}.api.riotgames.com";
        }
    }
}