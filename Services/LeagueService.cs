﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LeagueBot.Models;

namespace LeagueBot.Services
{
    public class LeagueService
    {
        private readonly RiotApiService riotApiService;
        private readonly SummonersService summonersService;

        public LeagueService(RiotApiService riotApiService, SummonersService summonersService)
        {
            this.riotApiService = riotApiService;
            this.summonersService = summonersService;
        }

        public async Task<IEnumerable<LeagueDto>> GetBySummonerId(string summonerId, string region)
        {
            using var client = this.riotApiService.GetClient(region);
            return await client.GetFromJsonAsync<IEnumerable<LeagueDto>>(
                $"/lol/league/v4/entries/by-summoner/{summonerId}");
        }

        public async Task<LeagueDto> GetSoloLeagueBySummonerId(string summonerId, string region)
        {
            var leagues = await GetBySummonerId(summonerId, region);
            return leagues.FirstOrDefault(l => l.QueueType == LeagueDto.SoloType);
        }

        public async Task<LeagueDto> GetSoloLeagueByName(string name, string region)
        {
            var summoner = await this.summonersService.GetByName(name, region);
            return await GetSoloLeagueBySummonerId(summoner.Id, region);
        }
    }
}