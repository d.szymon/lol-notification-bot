﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using LeagueBot.Models;
using LeagueBot.Repositories;
using Microsoft.Extensions.Logging;

namespace LeagueBot.Services
{
    public class RankComparisonService
    {
        private readonly DiscordService discordService;
        private readonly ILogger<RankComparisonService> logger;
        private readonly PlayersRepository playersRepository;
        private readonly LeagueService leagueService;

        public RankComparisonService(DiscordService discordService, ILogger<RankComparisonService> logger, PlayersRepository playersRepository, LeagueService leagueService)
        {
            this.discordService = discordService;
            this.logger = logger;
            this.playersRepository = playersRepository;
            this.leagueService = leagueService;
        }

        public async Task CompareRanks(OpGgListPlayer player)
        {
            var existingPlayer = await this.playersRepository.GetByNameAndRegion(player.Region, player.Name);

            if (existingPlayer == null)
            {
                if (player.Rank == LeagueRank.TargetRank)
                {
                    player.Notified = true;
                }
                
                await this.playersRepository.Insert(player);
                return;
            }

            if (existingPlayer.Notified)
            {
                return;
            }

            if (player.Rank <= LeagueRank.TargetRank)
            {
                var league = await this.leagueService.GetSoloLeagueByName(player.Name, player.Region);
                await this.playersRepository.UpdateNotified(existingPlayer.Id);
                await this.discordService.SendRankNotification(player, league);
                this.logger.LogInformation("Wysłano powiadomienie o zmianie rangi");
            }

            if (existingPlayer.Rank != player.Rank)
            {
                await this.playersRepository.UpdateRank(existingPlayer.Id, player.Rank);
                this.logger.LogInformation("Zaktualizowano rangęg gracza {summonerName}", player.Name);
            }
        }
    }
}