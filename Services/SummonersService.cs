﻿using System;
using System.Collections.Concurrent;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LeagueBot.Models;

namespace LeagueBot.Services
{
    public class SummonersService
    {
        private readonly RiotApiService riotApiService;
        private readonly ConcurrentDictionary<string, SummonerDto> cache = new();
        
        public SummonersService(RiotApiService riotApiService)
        {
            this.riotApiService = riotApiService;
        }

        public async Task<SummonerDto> GetByName(string name, string region)
        {
            if (cache.TryGetValue(name, out var dto))
            {
                return dto;
            }

            using var client = this.riotApiService.GetClient(region);
            dto = await client.GetFromJsonAsync<SummonerDto>($"/lol/summoner/v4/summoners/by-name/{Uri.EscapeDataString(name)}");

            cache.TryAdd(name, dto);
            
            return dto;
        }
    }
}