﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LeagueBot.Models;

namespace LeagueBot.Services
{
    public class OpGgService
    {
        private readonly IHttpClientFactory httpClientFactory;

        public OpGgService(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        public async Task<IEnumerable<OpGgListPlayer>> GetPage(string region, uint page)
        {
            using var client = this.httpClientFactory.CreateClient(nameof(OpGgService));
            client.BaseAddress = new Uri(GetUrlFromRegion(region));
            var response = await client.GetStringAsync($"/ranking/ladder/page={page}");
            var matches =
                new Regex(
                    "opgg-static\\.akamaized\\.net\\/images\\/profile_icon.*\"><span>(.*?)<\\/span.*ranking-table__cell--tier.*\\n\\s*(.*?)\\n.*cell--lp\">\\n\\s*(.*?) LP",
                    RegexOptions.Multiline | RegexOptions.ECMAScript).Matches(response);

            var list = new List<OpGgListPlayer>(matches.Count);
            
            foreach (Match match in matches)
            {
                var player = new OpGgListPlayer()
                {
                    LeaguePoints = int.Parse(match.Groups[3].Value),
                    Name = match.Groups[1].Value.Trim(),
                    Region = region,
                    Rank = new LeagueRank(match.Groups[2].Value.Trim()),
                };
                list.Add(player);
            }

            return list;
        }

        public async Task<uint> GetTotalPages(string region)
        {
            using var client = this.httpClientFactory.CreateClient(nameof(OpGgService));
            client.BaseAddress = new Uri(GetUrlFromRegion(region));
            var response = await client.GetStringAsync("/ranking/ladder/page=1");
            var match = new Regex("Total <span>(.*?)<\\/span>").Match(response);
            var total = uint.Parse(match.Groups[1].Value.Replace(".", string.Empty).Replace(",", string.Empty));
            return (uint)Math.Ceiling((double)total / 100);
        }

        public static string GetOpGgRegionFromRiotRegion(string region)
        {
            return region.ToUpper() switch
            {
                "EUN1" => "eune",
                "BR1" => "br",
                "EUW1" => "euw",
                "JP1" => "jp",
                "KR" => "www",
                "LA1" => "las",
                "LA2" => "lan",
                "NA1" => "na",
                "OC1" => "oce",
                "RU" => "ru",
                "TR1" => "tr",
                _ => throw new ArgumentException($"Nieznany region: {region}", nameof(region)),
            };
        }

        public static string GetUrlFromRegion(string region)
        {
            return $"https://{GetOpGgRegionFromRiotRegion(region)}.op.gg";
        }
    }
}