﻿using LeagueBot.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace LeagueBot.Contexts
{
    public class DatabaseContext
    {
        private readonly IMongoDatabase database;

        public DatabaseContext(IOptions<DbSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            this.database = client.GetDatabase("lol");
        }

        public IMongoCollection<OpGgListPlayer> Players => database.GetCollection<OpGgListPlayer>("players");
    }
}