﻿using System.Threading.Tasks;
using LeagueBot.Contexts;
using LeagueBot.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace LeagueBot.Repositories
{
    public class PlayersRepository
    {
        private readonly DatabaseContext databaseContext;

        public PlayersRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public Task<OpGgListPlayer> GetByNameAndRegion(string region, string name)
        {
            return this.databaseContext.Players.Find(p => p.Region == region && p.Name == name).FirstOrDefaultAsync();
        }

        public Task UpdateRank(ObjectId id, LeagueRank rank)
        {
            return this.databaseContext.Players.UpdateOneAsync(p => p.Id == id,
                Builders<OpGgListPlayer>.Update.Set(p => p.Rank, rank));
        }

        public Task Insert(OpGgListPlayer player)
        {
            return this.databaseContext.Players.InsertOneAsync(player);
        }

        public Task UpdateNotified(ObjectId id)
        {
            return this.databaseContext.Players.UpdateOneAsync(p => p.Id == id,
                Builders<OpGgListPlayer>.Update.Set(p => p.Notified, true));
        }
    }
}