﻿using System;

namespace LeagueBot.Models
{
    public class LeagueRank
    {
        public string Tier { get; set; }

        public byte Rank { get; set; }

        public byte NumericTier 
        {
            get
            {
                return Tier.ToLower() switch
                {
                    "iron" => 1,
                    "bronze" => 2,
                    "silver" => 3,
                    "gold" => 4,
                    "platinum" => 5,
                    "diamond" => 6,
                    "master" => 7,
                    "grandmaster" => 8,
                    "challenger" => 9,
                    _ => throw new ArgumentException($"Nieznana ranga: {Tier}", nameof(Tier)),
                };
            }
        }

        public static LeagueRank TargetRank => new("Iron 4");
        
        public LeagueRank() {}

        public LeagueRank(string tier, byte rank)
        {
            this.Tier = tier.ToLower();
            this.Rank = rank;
        }

        public LeagueRank(string tier, string rank) : this(tier, ParseRank(rank))
        {
            
        }

        public LeagueRank(string league) : this(league[..league.IndexOf(' ')],
            league[(league.IndexOf(' ') + 1)..])
        {
            
        }

        public static byte ParseRank(string rank)
        {
            return rank.ToLower() switch
            {
                "I" => 1,
                "II" => 2,
                "III" => 3,
                "IV" => 4,
                _ => byte.Parse(rank)
            };
        }

        public override string ToString()
        {
            return $"{Tier} {Rank}";
        }

        protected bool Equals(LeagueRank other)
        {
            return Tier == other.Tier && Rank == other.Rank;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((LeagueRank) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Tier, Rank);
        }

        public static bool operator ==(LeagueRank rank1, LeagueRank rank2)
        {
            if (rank1 is null || rank2 is null)
            {
                if (rank2 is null)
                {
                    return true;
                }

                return false;
            }

            return rank1.Rank == rank2.Rank && rank1.Tier == rank2.Tier;
        }

        public static bool operator !=(LeagueRank rank1, LeagueRank rank2) => !(rank1 == rank2);

        public static bool operator <(LeagueRank rank1, LeagueRank rank2)
        {
            return rank1.NumericTier < rank2.NumericTier ||
                   (rank1.NumericTier == rank2.NumericTier && rank1.Rank > rank2.Rank);
        }

        public static bool operator >(LeagueRank rank1, LeagueRank rank2) => !(rank1 < rank2) && rank1 != rank2;

        public static bool operator <=(LeagueRank rank1, LeagueRank rank2) => rank1 < rank2 || rank1 == rank2;
        public static bool operator >=(LeagueRank rank1, LeagueRank rank2) => rank1 > rank2 || rank1 == rank2;
    }
}