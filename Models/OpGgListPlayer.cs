﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace LeagueBot.Models
{
    public class OpGgListPlayer
    {
        [BsonId] 
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }
        public int LeaguePoints { get; set; }
        public LeagueRank Rank { get; set; }
        public bool Notified { get; set; }

        public OpGgListPlayer()
        {
            this.Notified = false;
        }
    }
}