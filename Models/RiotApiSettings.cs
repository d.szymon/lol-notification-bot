﻿using System.Collections.Generic;
using System.Linq;

namespace LeagueBot.Models
{
    public class RiotApiSettings
    {
        public string ApiKey { get; }

        public RiotApiSettings(string apiKey)
        {
            this.ApiKey = apiKey;
        }
    }
}