﻿namespace LeagueBot.Models
{
    public class DiscordSettings
    {
        public string Token { get; }

        public DiscordSettings(string token)
        {
            Token = token;
        }
    }
}