﻿using System.Collections.Generic;

namespace LeagueBot.Models
{
    public class OpGgSettings
    {
        public Dictionary<string, SingleServerSettings> Servers { get; set; }

        public class SingleServerSettings
        {
            public uint StartPage { get; set; }       
            public string Channel { get; set; }
        }
    }
}