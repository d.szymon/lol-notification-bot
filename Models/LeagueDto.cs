﻿using System;

namespace LeagueBot.Models
{
    public class LeagueDto
    {
        public const string SoloType = "RANKED_SOLO_5x5";
        
        public string LeagueId { get; set; }
        public string SummonerId { get; set; }
        public string SummonerName { get; set; }
        public string QueueType { get; set; }
        public string Tier { get; set;}
        public string Rank { get; set; }
        public int LeaguePoints { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public bool HotStreak { get; set; }
        public bool Veteran { get; set; }
        public bool FreshBlood { get; set; }
        public bool Inactive { get; set; }

        public bool IsTargetRank => Tier == "IRON" && Rank == "IV";

        public int TotalGames => Wins + Losses;
        public int WinRatio => (int)Math.Round(TotalGames == 0 ? 100 : ((double)Wins / TotalGames * 100));
    }
}