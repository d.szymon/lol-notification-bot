using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using LeagueBot.Contexts;
using LeagueBot.Models;
using LeagueBot.Repositories;
using LeagueBot.Services;

namespace LeagueBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    services
                        .AddHttpClient()
                        .AddSingleton<SummonersService>()
                        .AddSingleton<RiotApiService>()
                        .AddSingleton<LeagueService>()
                        .AddSingleton<DiscordService>()
                        .AddSingleton<RankComparisonService>()
                        .Configure<OpGgSettings>(hostContext.Configuration.GetSection("OpGG"))
                        .AddSingleton(t => new DiscordSettings(Environment.GetEnvironmentVariable("DISCORD_BOT_TOKEN")))
                        .AddSingleton<OpGgService>()
                        .AddSingleton(t => new RiotApiSettings(Environment.GetEnvironmentVariable("LOL_API_KEY")))
                        .AddSingleton<DatabaseContext>()
                        .AddSingleton<PlayersRepository>()
                        .Configure<DbSettings>(hostContext.Configuration.GetSection("Database"))
                        .Configure<OpGgSettings>(hostContext.Configuration.GetSection("OpGG"))
                        .PostConfigure<OpGgSettings>((settings) =>
                        {
                            settings.Servers =
                                JsonSerializer.Deserialize<Dictionary<string, OpGgSettings.SingleServerSettings>>(
                                    File.ReadAllText(hostContext.Configuration.GetSection("OpGG")
                                        .GetSection("ServersFile").Value));
                        });
                });
    }
}
