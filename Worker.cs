using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeagueBot.Models;
using LeagueBot.Services;
using Microsoft.Extensions.Options;

namespace LeagueBot
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> logger;
        private readonly DiscordService discordService;
        private readonly OpGgSettings opGgSettings;
        private readonly OpGgService opGgService;
        private readonly RankComparisonService rankComparisonService;

        public Worker(ILogger<Worker> logger,
            DiscordService discordService, IOptions<OpGgSettings> opGgSettings, OpGgService opGgService, RankComparisonService rankComparisonService)
        {
            this.logger = logger;
            this.discordService = discordService;
            this.opGgSettings = opGgSettings.Value;
            this.opGgService = opGgService;
            this.rankComparisonService = rankComparisonService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                await this.discordService.Init();
                logger.LogInformation("Zalogowano do Discorda");
                while (!stoppingToken.IsCancellationRequested)
                {
                    var tasks = opGgSettings.Servers.Select(item => HandleRegion(item.Key, item.Value, stoppingToken)).ToList();
                    await Task.WhenAll(tasks);
                    await Task.Delay(TimeSpan.FromMinutes(2), stoppingToken);
                }
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "o boze");
            }
        }

        protected async Task HandleRegion(string region, OpGgSettings.SingleServerSettings settings, CancellationToken cancellationToken)
        {
            var page = settings.StartPage;
            var totalPages = await this.opGgService.GetTotalPages(region);
            this.logger.LogInformation("Ilość stron na {Region} wynosi {PagesCount}", region, totalPages);
            
            while (!cancellationToken.IsCancellationRequested)
            {
                var players = await this.opGgService.GetPage(region, page);
                
                foreach (var player in players)
                {
                    await this.rankComparisonService.CompareRanks(player);
                }
                
                this.logger.LogInformation("{Region}: {Page}/{Total}", region, page, totalPages);
                
                page++;

                if (page >= totalPages)
                {
                    page = settings.StartPage;
                    await this.discordService.SendCompletedNotification(region, page);
                }
                
                await Task.Delay(TimeSpan.FromSeconds(2), cancellationToken);
            }
        }
    }
}
